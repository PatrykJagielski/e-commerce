import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Main from './Pages/Main'
import SignIn from './Pages/SignIn';
import SignUp from './Pages/SignUp';
import Item from './Pages/Item';
import Cart from './Pages/Cart'
import NavBar from './Components/NavBar';

function App() {
  return (
    <Router>
        <NavBar />
          <Routes>
            <Route exact path='/' element={<Main/>} />
            <Route path='/login' element={<SignIn />} />
            <Route path='/signup' element={<SignUp />} />
            <Route path='/item/:id' element={<Item />} />
            <Route path='/cart' element={<Cart />} />
          </Routes>
    </Router>
  );
}

export default App;
