import axios from "axios";


export default function RefreshToken(token) {
  axios.post(process.env.REACT_APP_BACKEND_URL + '/auth/token/', {
    grant_type: "refresh_token",
    refresh_token: token,
    client_id: process.env.REACT_APP_CLIENT_ID,
    client_secret: process.env.REACT_APP_CLIENT_SECRET
  })
  .then((res) => {
    if(res.status === 200) {
      localStorage.setItem('access_token', res.data.access_token);
      localStorage.setItem('refresh_token', res.data.refresh_token);
    }
  })
  .catch((e) => {
    console.log(e);
  })
}