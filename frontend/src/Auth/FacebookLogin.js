import axios from 'axios';

const FacebookLogin = (accessToken, navigate) => {
	axios.post(process.env.REACT_APP_BACKEND_URL + '/auth/convert-token', {
    token: accessToken,
    backend: 'facebook',
    grant_type: 'convert_token',
    client_id: process.env.REACT_APP_CLIENT_ID,
    client_secret: process.env.REACT_APP_CLIENT_SECRET,
  })
  .then((res) => {
    console.log(res)
    localStorage.setItem('access_token', res.data.access_token);
    localStorage.setItem('refresh_token', res.data.refresh_token);
    navigate('/');
    window.location.reload(true);
  });
};

export default FacebookLogin;