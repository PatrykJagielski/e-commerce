import axios from 'axios';
import { useNavigate } from "react-router-dom";
import Badge from '@mui/material/Badge';
import Button from '@mui/material/Button';
import { AppBar, Box, IconButton, Toolbar, Typography, Avatar } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { useEffect, useState } from "react";


export default function NavBar() {
  const navigate = useNavigate();
  const [authenticated, setAuthenticated] = useState(false)
  const [avatar, setAvatar] = useState("")
  const [cartCount, setCartCount] = useState(0)

  useEffect(() => {
    const access_token = localStorage.getItem('access_token');
    if (access_token) {
      setAuthenticated(true);
      axios.get(process.env.REACT_APP_BACKEND_URL + '/users/avatar/', {
        headers: {
          Authorization: 'Bearer ' + access_token
        }
       })
      .then((res) => {
        if(res.status === 200) {
          setAvatar(process.env.REACT_APP_BACKEND_URL + res.data.avatar);
        }
      })
      .catch((e) => {
        console.log(e);
      });

      axios.get(process.env.REACT_APP_BACKEND_URL + '/cart/count/', {
        headers: {
          Authorization: 'Bearer ' + access_token
        }
       })
       .then(res => {
        setCartCount(res.data.count)
       })
       .catch((e) => {
        console.log(e);
      });
    } else {
      setAuthenticated(false);
    }
  }, [])
  
  const handleLogout = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    setAuthenticated(false);
    navigate('/')
  }

  return (
    <AppBar>
      <Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
        <IconButton>
          <MenuIcon />
        </IconButton>
        <Typography variant='h5' style={{cursor: "pointer"}} onClick={() => navigate('/')}>
          e-Commerce
        </Typography>
        {authenticated ? 
        <Box sx={{display: 'flex'}} onClick={() => navigate('/cart')}>
          <IconButton aria-label="cart" sx={{paddingRight: '20px'}}>
            <Badge badgeContent={cartCount} color="secondary">
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
          <Avatar alt="avatar" src={avatar} sx={{marginRight: 1}}/>
          <Button variant="contained" color="secondary" onClick={handleLogout}>
            Logout
          </Button>
        </Box> 
        : 
        <Box>
          <Button variant="contained" color="secondary" onClick={() => navigate('/login')}>
            Sign In
          </Button>
          <Button variant="contained" color="secondary" sx={{ marginLeft: 1 }} onClick={() => navigate('/signup')}>
            Sing Up
          </Button>
        </Box>
        }
      </Toolbar>
    </AppBar>
  )
}