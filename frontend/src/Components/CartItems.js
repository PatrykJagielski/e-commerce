import axios from 'axios';
import { Box, Divider, Link, List, ListItem, TextField, Typography } from "@mui/material";
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';
import RefreshToken from '../Auth/RefreshToken';


export default function CartItems({items, setItems, setCartUpdated}) {

  const handleRemoveItem = (id) => {
    const access_token = localStorage.getItem('access_token');
    axios.delete(process.env.REACT_APP_BACKEND_URL + `/cart/remove-item/${id}/`, 
      {headers: {Authorization: 'Bearer ' + access_token},}
    )
    .then((res) => {
      if(res.status === 204) {
        setItems(items.filter(item => item.id !== id))
      }
    })
    .catch((e) => {
      if(e.response.status === 401) {
        const access_token = localStorage.getItem('access_token');
        RefreshToken(access_token)
      }
    })
  }

  const handleUpdateAmount = (e, id) => {
    setItems(items.map(((item) => {
      if(item.item.id === id) {
        item.amount = e.target.value;
        item.updated = true;
      }
      return item;
    })));
    setCartUpdated(true);
  }

  return (
    <Box>
      <List>
        {
          items.map(({id, item, amount}) => (
            <>
              <ListItem sx={{display: "flex", justifyContent: "space-between"}}>
                <Link href={'/item/' + item.id} underline="hover" sx={{width: "50%"}}>
                  {item.name}
                </Link>
                <TextField
                  id="outlined-number"
                  type="number"
                  value={amount}
                  onChange={(e) => handleUpdateAmount(e, item.id)}
                  size="small"
                  inputProps={{ inputMode: 'numeric', pattern: '[0-9]*', min: '0' }}
                  sx={{width: "80px", paddingX: "10px"}}
                  />
                <Typography>
                  {item.price + ' zł'}
                </Typography>
                <DeleteForeverOutlinedIcon 
                  sx={{cursor: "pointer"}}
                  onClick={() => handleRemoveItem(id)}
                />
              </ListItem>
              <Divider />
            </>
          ))
        }
      </List>
    </Box>
  )
}