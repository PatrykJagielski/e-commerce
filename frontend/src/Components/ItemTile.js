import { CardMedia, Grid, Link, Paper, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

export default function ItemTile({item}) {

  const navigate = useNavigate()

  const handleNavigate = (e) => {
    e.preventDefault();
    navigate('/item/' + item.id);
  }

  return (
    <Grid item xs={6} sm={4} md={3}>
      <Paper elevation={3}>
        <CardMedia image={item.image} sx={{aspectRatio: '16 / 9'}}/>
        <Typography variant="h5">
          <Link href={'/item/' + item.id} onClick={handleNavigate} underline="hover">
            {item.name}
          </Link>
        </Typography>
        <Typography paragraph>
          {item.price + ' $'}
        </Typography>
      </Paper>
    </Grid>
  )
}