import { Box, Button, Divider, FormControl, FormControlLabel, Grid, Paper, Radio, RadioGroup, Typography } from "@mui/material";
import AddLocationIcon from '@mui/icons-material/AddLocation';
import { useEffect, useState } from "react";


export default function CartSummary({summaryPrice}) {
  const [paczkomat, setPaczkomat] = useState();
  const [shipping, setShipping] = useState('paczkomat');

  useEffect(() => {
    const paczkomatLocalStorage = JSON.parse(localStorage.getItem('paczkomat'));
    if(paczkomatLocalStorage) {
      setPaczkomat(paczkomatLocalStorage);
    }
  }, [])

  const openModal = () => {
    window.easyPack.modalMap(function(point, modal) {
      modal.closeModal();
      console.log(point);
      setPaczkomat(point);
      localStorage.setItem('paczkomat', JSON.stringify(point))
    }, { width: 600, height: 600 });
  };

  const round = (x) => {
    return Math.round((x * 100)) / 100
  }

  return (
    <Grid item xs={12} sm={6} md={5}>
      <Paper elevation={3} sx={{maxWidth: "300px", padding: "10px"}}>
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Typography variant="h6">
            Kwota: 
          </Typography>
          <Typography variant="h6">
            {summaryPrice + ' zł'}
          </Typography>
        </Box>
        <Divider sx={{marginY: "10px"}} />
        <Typography variant="h6">
          Wysyłka
        </Typography>
        <FormControl fullWidth>
          <RadioGroup defaultValue="paczkomat">
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
              <FormControlLabel 
                value="paczkomat" control={<Radio />} label="Paczkomat" 
                onClick={() => setShipping('paczkomat')} />
              <Typography>14.99 zł</Typography>
            </Box>
            <Box sx={{marginLeft: "30px"}}>
              <Button variant="contained" onClick={openModal} disabled={shipping !== 'paczkomat'} >
                <AddLocationIcon sx={{marginRight: "10px"}} />
                {paczkomat ? "Zmień Paczkomat" : "Wybierz Paczkomat"}
              </Button>
                {paczkomat ? 
                <Typography variant="body2" sx={{marginTop: "5px"}}>
                  Wybrany paczkomat: <br />
                  {paczkomat.name} <br />
                  {paczkomat.address.line1} <br />
                  {paczkomat.address.line2}
                </Typography>
                : null}
            </Box>
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
              <FormControlLabel 
                value="kurier" control={<Radio />} label="Kurier"
                onClick={() => setShipping('kurier')} />
              <Typography>
                19.99 zł
              </Typography>
            </Box>
          </RadioGroup>
        </FormControl>
        <Divider sx={{marginTop: "5px"}} />
        <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: "center" }}>
          <Typography variant="h6">
            Łącznie: 
          </Typography>
          <Typography>
            {shipping === 'paczkomat' ? round(summaryPrice + 14.99) : round(summaryPrice + 19.99)} zł
          </Typography>
        </Box>
      </Paper>
    </Grid>
  );
};