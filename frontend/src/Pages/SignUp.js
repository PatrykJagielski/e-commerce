import axios from 'axios';
import { Button, TextField, FormHelperText, Box, Avatar, Typography } from "@mui/material";
import { useState } from "react";
import '../App.css'
import FbLogin from 'react-facebook-login';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { useNavigate } from "react-router-dom";
import FacebookLogin from '../Auth/FacebookLogin';
import { Container } from '@mui/system';


export default function SignIn() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorPassword, setErrorPassword] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const navigate = useNavigate();

  const responseFacebook = async (response) => {
		FacebookLogin(response.accessToken, navigate);
	}

  const handleSignUp = () => {
    axios.post(process.env.REACT_APP_BACKEND_URL + '/users/create/', {
      email: email,
      password: password
    })
    .then((res) => {
      if (res.status === 201) {
        navigate("/login");
      }
    })
    .catch((e) => {
      setErrorEmail(e.response.data.email);
      setErrorPassword(e.response.data.password);
    });
  };

  return (
    <>
      <Container sx={{marginTop: 11}} component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
          <Box sx={{ mt: 1 }}>
            {errorEmail && <FormHelperText error={true}>{errorEmail}</FormHelperText>}
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={(e) => setEmail(e.target.value)} value={email}
              />
            {errorPassword && <FormHelperText error={true}>{errorPassword}</FormHelperText>}
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => setPassword(e.target.value)} value={password}
              />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleSignUp}
              >
              Sign Up
            </Button>
            <FbLogin
              buttonStyle={{width: "100%"}}
              appId={process.env.REACT_APP_APP_ID}
              fields="name,email,picture"
              callback={responseFacebook}
            />
          </Box>
        </Box>
      </Container>
    </>
  )
}