import axios from 'axios';
import '../App.css'
import { useEffect, useState } from 'react';
import { Grid, Container } from '@mui/material';
import ItemTile from '../Components/ItemTile';

export default function Main() {
  const [items, setItems] = useState([])

  useEffect(() => {
    axios.get(process.env.REACT_APP_BACKEND_URL + '/items/items/')
    .then((res) => {
      if(res.status === 200) {
        setItems(res.data);
      }
    })
  }, [])

  return (
    <>
      <Container sx={{marginY: 11}}>
        <Grid container spacing={2}>
          {
            items.map((item) => (
              <ItemTile item={item}/> 
            ))
          }
        </Grid>
      </Container>
    </>
  )
}