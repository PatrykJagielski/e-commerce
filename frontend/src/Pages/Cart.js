import axios from 'axios';
import '../App.css'
import { useEffect, useState } from 'react';
import { Button, Container, Grid } from '@mui/material';
import CartSummary from '../Components/CartSummary';
import CartItems from '../Components/CartItems';
import RefreshToken from '../Auth/RefreshToken';


export default function Item() {
  const [items, setItems] = useState([]);
  const [summaryPrice, setSummaryPrice] = useState(.0);
  const [cartUpdated, setCartUpdated] = useState(false);
  
  useEffect(() => {
    const access_token = localStorage.getItem('access_token');
    axios.get(process.env.REACT_APP_BACKEND_URL + '/cart/cart/', {
      headers: {
        Authorization: 'Bearer ' + access_token
      }
     })
    .then((res) => {
      if(res.status === 200) {
        setItems(res.data.items);
        setSummaryPrice(res.data.summary_price);
      }
    })
    .catch((e) => {
      const refresh_token = localStorage.getItem('refresh_token')
      if(e.response.status === 401 && refresh_token) {
        RefreshToken(refresh_token);
      }
    })
  }, []);

  useEffect(() => {
    window.easyPackAsyncInit = function () {
      window.easyPack.init({
        defaultLocale: 'pl',
        mapType: 'osm',
        searchType: 'osm',
        points: {
          types: ['parcel_locker']
        },
        map: {
          initialTypes: ['parcel_locker']
        }
      });
    };
  }, []);

  const handleUpdateCart = () => {
    const access_token = localStorage.getItem('access_token');
    var data = []
    items.map((item) => {
      if(item.updated) {
        data.push({id: item.id, amount: item.amount})
      }
      return item;
    });
    console.log(data);

    axios.put(process.env.REACT_APP_BACKEND_URL + '/cart/update/', {
      items: data,
    }, {headers: {
      Authorization: 'Bearer ' + access_token
    },})
    .then((res) => {
      if(res.status === 200) {
        setItems(res.data.items);
        setSummaryPrice(res.data.summary_price);
        setCartUpdated(false);
      }
    })
    .catch((e) => {
      console.log(e.response)
    })
  }

  return (
      <Container sx={{marginY: 11}} maxWidth="md">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={7}> 
            <CartItems items={items} setItems={setItems} setCartUpdated={setCartUpdated} />
            <Button 
              variant="contained" 
              disabled={!cartUpdated}
              onClick={handleUpdateCart}
            >
              Zaktualizuj Koszyk
            </Button>
          </Grid>
          <CartSummary summaryPrice={summaryPrice} />
        </Grid>
      </Container>
  );
};