import axios from 'axios';
import '../App.css'
import { useState, useEffect } from 'react';
import { Box, Button, CardMedia, Container, Grid, TextField, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';


export default function Item() {
  const [item, setItem] = useState([])
  const [amount, setAmount] = useState('1')
  const {id} = useParams()

  useEffect(() => {
    axios.get(process.env.REACT_APP_BACKEND_URL + '/items/items/' + id)
    .then((res) => {
      if(res.status === 200) {
        setItem(res.data);
      }
    })
  }, [id,])

  const handleAddToCart = () => {
    const access_token = localStorage.getItem('access_token');
    axios.post(process.env.REACT_APP_BACKEND_URL + '/cart/add-item/', {
      item: id,
      amount: amount
    }, {headers: {
      Authorization: 'Bearer ' + access_token
    },})
    .then(res => {
      if(res.status === 201) {
        console.log('item added to cart')
      }
    })
    .catch(e => console.log(e))
  }

  return (
    <>
      <Container sx={{marginY: 11}}>
        <Grid container spacing={2}>
          <Grid item>
            <CardMedia image={item.image} sx={{height: "300px", aspectRatio: "16 / 9"}}/>
          </Grid>
          <Grid item>
            <Typography variant="h2">{item.name}</Typography>
            <Typography variant="h5">{item.description}</Typography>
            <Typography variant="h6">{item.price} $</Typography>
            <Box
              marginTop={1}
              component="form"
              noValidate
              autoComplete="off"
              >
              <TextField
                type="number"
                label="Amount"
                defaultValue={1}
                value={amount}
                size="small"
                onChange={(e) => setAmount(e.target.value)} 
                inputProps={{ inputMode: 'numeric', pattern: '[0-9]*', min: '1' }}
                />
              <Button size="medium" variant="contained" onClick={handleAddToCart} sx={{marginLeft: 1}}>
                Add to cart
              </Button>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}