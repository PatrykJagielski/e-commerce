import axios from 'axios';
import { Avatar, Box, Button, Container, FormHelperText, Grid, Link, TextField, Typography } from "@mui/material";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import FbLogin from 'react-facebook-login';
import FacebookLogin from "../Auth/FacebookLogin";
import '../App.css'
import { useNavigate } from "react-router-dom";
import { useState } from 'react';

export default function SignIn() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("")
  const navigate = useNavigate();

  const responseFacebook = async (response) => {
		FacebookLogin(response.accessToken, navigate);
	}

  const handleSignIn = () => {
    axios.post(process.env.REACT_APP_BACKEND_URL + '/auth/token/', {
      grant_type: "password",
      username: email,
      password: password,
      client_id: process.env.REACT_APP_CLIENT_ID,
      client_secret: process.env.REACT_APP_CLIENT_SECRET
    })
    .then((res) => {
      if (res.status === 200) {
        localStorage.setItem('access_token', res.data.access_token);
        localStorage.setItem('refresh_token', res.data.refresh_token);
        navigate("/");
      }
    })
    .catch((e) => {
      if (e.response.status === 400) {
        setError("invalid credentials.")
      }
      else {
        setError("unknown error.")
      }
    });
  };

  return (
    <>
      <Container sx={{marginY: 11}} component="main" maxWidth="xs">
        <Box
        sx={{
          marginTop: 8,
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
        >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
          <Box sx={{ mt: 1 }}>
            {error && <FormHelperText error={true}>{error}</FormHelperText>}
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={(e) => setEmail(e.target.value)} value={email}
              />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={(e) => setPassword(e.target.value)} value={password}
              />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleSignIn}
              >
              Sign In
            </Button>
            <FbLogin
              buttonStyle={{width: "100%"}}
              appId={process.env.REACT_APP_APP_ID}
              fields="name,email,picture"
              callback={responseFacebook}
              />
            <Grid container marginTop={1}>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
        </Box>
        </Box>
      </Container>
    </>
  )
}