from dataclasses import fields
from unicodedata import category
from rest_framework.serializers import ModelSerializer
from .models import Category, Item

class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name',)


class ItemListSerializer(ModelSerializer):
    class Meta:
        model = Item
        fields = ('id', 'name', 'price', 'image',)


class ItemSerializer(ModelSerializer):
    category = CategorySerializer()
    class Meta:
        model = Item
        fields = ('id', 'name', 'description', 'category', 'stock', 'price', 'image',)


class ItemSmallSerializer(ModelSerializer):
    class Meta:
        model = Item
        fields = ('id', 'name', 'price', 'image')
