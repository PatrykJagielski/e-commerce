from django.urls import path
from .views import ItemListView, ItemRetrieveView

app_name = 'items'

urlpatterns = [
    path('items/', ItemListView.as_view(), name='items'),
    path('items/<pk>/', ItemRetrieveView.as_view(), name='item-retrieve'),
]
