from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Item, Category


class TestItems(APITestCase):
    def setUp(self):
        self.category = Category.objects.create(name='category')
        self.item = Item.objects.create(name='item', description='description', 
                                        price=9.99, stock=10, category=self.category)
        
    def test_item_list(self):
        url = reverse('items:items')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_item_retrieve(self):
        url = reverse('items:item-retrieve', kwargs={'pk': self.item.id})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.item.id)
        self.assertEqual(response.data['name'], self.item.name)
