from django.urls import path
from .views import CartView, AddItemView, CartCount, RemoveItemView, UpdateItemAmountView

app_name = 'cart'

urlpatterns = [
    path('cart/', CartView.as_view(), name='cart'),
    path('add-item/', AddItemView.as_view(), name='add-item'),
    path('remove-item/<pk>/', RemoveItemView.as_view(), name='remove-item'),
    path('update/', UpdateItemAmountView.as_view(), name='update-items'),
    path('count/', CartCount.as_view(), name='count')
]
