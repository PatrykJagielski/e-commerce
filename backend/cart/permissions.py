from rest_framework.permissions import BasePermission

class IsCartOwner(BasePermission):

    def has_object_permission(self, request, view, obj):
        # Instance must have an attribute named `owner`.
        return obj.cart.user == request.user
