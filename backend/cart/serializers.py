from rest_framework.serializers import ModelSerializer, FloatField, IntegerField
from items.serializers import ItemSmallSerializer
from .models import Cart, CartItem


class AddItemSerializer(ModelSerializer):
    amount = IntegerField(min_value=1)
    
    class Meta:
        model = CartItem
        fields = ('item', 'amount', 'cart',)
        extra_kwargs = {'cart': {'write_only': True}}


class CartItemSerializer(ModelSerializer):
    item = ItemSmallSerializer()

    class Meta:
        model = CartItem
        fields = ('id', 'item', 'amount',)


class CartSerializer(ModelSerializer):
    items = CartItemSerializer(many=True)
    summary_price = FloatField(read_only=True)
    
    class Meta:
        model = Cart
        fields = ('items', 'summary_price',)
        extra_kwargs = {'summary_price': {'read_only': True}}


class CartItemAmountSerializer(ModelSerializer):
    item = ItemSmallSerializer(required=False, read_only=True)

    class Meta:
        model = CartItem
        fields = ('id', 'item', 'amount',)
        extra_kwargs = {
            'id': {'read_only': False},
        }


class CartUpdateSerializer(ModelSerializer):
    items = CartItemAmountSerializer(many=True)
    summary_price = FloatField(read_only=True)
    
    class Meta:
        model = Cart
        fields = ('items', 'summary_price',)
        extra_kwargs = {'summary_price': {'read_only': True}}

    def update(self, instance, validated_data):
        items = validated_data.get('items')
        for item in items:
            obj = instance.items.get(id=item['id'])
            obj.amount = item['amount']
            obj.save()
        return instance
