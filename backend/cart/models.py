from django.db import models


class CartItem(models.Model):
    item = models.ForeignKey('items.Item', on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(default=1)
    cart = models.ForeignKey('Cart', on_delete=models.CASCADE, related_name='items')

    def __str__(self):
        return f'{self.item.name} {self.item.price}'


class Cart(models.Model):
    user = models.OneToOneField('users.NewUser', on_delete=models.CASCADE, related_name='cart')

    @property
    def summary_price(self):
        sum = 0
        for item in self.items.all():
            sum += item.amount * item.item.price
        return round(sum,2)
    
    def __str__(self):
        return self.user.first_name
