from rest_framework import status
from rest_framework.generics import RetrieveAPIView, CreateAPIView, DestroyAPIView, UpdateAPIView, get_object_or_404
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from .models import Cart, CartItem
from .serializers import CartSerializer, AddItemSerializer, CartUpdateSerializer
from .permissions import IsCartOwner


class CartView(RetrieveAPIView):
    queryset = Cart.objects.all()
    serializer_class = CartSerializer
    permission_classes = [IsAuthenticated]
    
    def get_object(self):
        return self.request.user.cart


class RemoveItemView(DestroyAPIView):
    permission_classes = [IsAuthenticated, IsCartOwner]
    queryset = CartItem.objects.all()


class UpdateItemAmountView(UpdateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = Cart.objects.all()
    serializer_class = CartUpdateSerializer
    
    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())

        obj = get_object_or_404(
            queryset, user=self.request.user
        )

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj
    

class AddItemView(CreateAPIView):
    serializer_class = AddItemSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        request.data['cart'] = self.request.user.cart.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if self.request.user.cart.items.filter(item_id=request.data['item']).exists():
            obj = self.request.user.cart.items.filter(item_id=request.data['item']).first()
            obj.amount += int(request.data['amount'])
            obj.save()
        else:
            self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CartCount(APIView):
    permission_classes = [AllowAny]

    def get(self, request):
        return Response({'count': request.user.cart.items.count()})