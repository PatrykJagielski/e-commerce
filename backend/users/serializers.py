from enum import unique
from pyexpat import model
from statistics import mode
from rest_framework import serializers
from users.models import NewUser, Address


class CustomUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(min_length=8, write_only=True)

    class Meta:
        model = NewUser
        fields = ('email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        if self.Meta.model.objects.filter(email=validated_data['email']).exists():
            raise serializers.ValidationError({'email': 'User with that email already exists.'})
        password = validated_data.pop('password', None)
        # as long as the fields are the same, we can just use this
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ('address_line1', 'address_line2', 'zip_code', 'city')


class AvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = NewUser
        fields = ('avatar',)