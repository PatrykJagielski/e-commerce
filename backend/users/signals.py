from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import NewUser
from cart.models import Cart


@receiver(post_save, sender=NewUser)
def create_cart(sender, instance, **kwargs):
    if not hasattr(instance, 'cart'):
        Cart.objects.create(user=instance)