import base64
from tempfile import NamedTemporaryFile
from urllib.request import urlopen, urlretrieve
from django.core.files import File
from .models import NewUser

def save_profile(backend, user, response, is_new=False, *args, **kwargs):
    if is_new and backend.name == "facebook":
        # The main part is how to get the profile picture URL and then do what you need to do
        url = f"https://graph.facebook.com/{response['id']}/picture/?type=large&access_token={response['access_token']}"
        user = NewUser.objects.get(email=user)
        img_temp = NamedTemporaryFile(delete=True)
        img_temp.write(urlopen(url).read())
        img_temp.flush()
        user.avatar.save(f"image_{user.pk}.jpg", File(img_temp))
        user.save()
