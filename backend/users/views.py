from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import CustomUserSerializer, AvatarSerializer
from rest_framework.permissions import AllowAny, IsAuthenticated


class CustomUserCreate(APIView):
    permission_classes = [AllowAny]

    def post(self, request, format='json'):
        print(request)
        serializer = CustomUserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                json = serializer.data
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AvatarView(APIView):
    permission_classes = [IsAuthenticated]
    
    def get(self, request, format='json'):
        serializer = AvatarSerializer(instance=request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)
