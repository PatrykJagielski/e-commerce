from django.urls import path
from .views import CustomUserCreate, AvatarView

app_name = 'users'

urlpatterns = [
    path('create/', CustomUserCreate.as_view(), name="create_user"),
    path('avatar/', AvatarView.as_view(), name='avatar'),
]
